/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author Jorge
 */
public class TestSingleton {
    
    public static void main(String[] args){
        //Se obtiene la referencia al HistoryList
        HistoryList lista = HistoryList.getInstance();
        
        //Añadimos comandos
        lista.addCommand("Comando 1");
        lista.addCommand("Comando 2");
        lista.addCommand("Comando 3");
        lista.addCommand("Comando 4");
        System.out.println("Comandos de la lista: \n" + lista.toString());
        
        //Eliminamos comandos
        System.out.println("Comando a deshacer: " + lista.undoCommando());
        System.out.println("Comando a deshacer: " + lista.undoCommando());
        System.out.println("Comandos de la lista: \n" + lista.toString());
    }
    
}
