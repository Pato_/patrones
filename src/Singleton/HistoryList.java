/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Jorge
 */
public class HistoryList {
    private List history = Collections.synchronizedList(new ArrayList<String>());
    private static HistoryList instance = new HistoryList();
    
    //Constructor
    private HistoryList(){}
   
    public static HistoryList getInstance(){return instance;}
    
    public void addCommand(String command){
        history.add(command);
    }
    
    public Object undoCommando(){
        return history.remove(history.size()-1);
    }
    
   public String toString(){
       StringBuilder result = new StringBuilder();
       
       for(int i=0;i<history.size();i++){
           result.append(" ");
           result.append(history.get(i));
           result.append("\n");
       }
       
       return result.toString();
   }
    
}
